# logisteam

Hackhathon project for IF 2018

## Repositories

- logistapi: API relying onNode.js and mongoDB
- logistadmin: static Web UI relying on React, offering member management and fiche creation
- logistapp: mobile app to take pictures and else. Relies on React native